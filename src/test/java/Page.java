import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Page extends Testing {

    @FindBy(css = "test")
    private WebElement test;

    Page() {
        PageFactory.initElements(driver, this);
    }

    private int counter = 0;

    void numOfAds() {
        driver.get("http://moneyversed.com/valuable-items-in-abandoned-places/?as=799");

        findElement("#top-banner");
        findElement("iframe[data-kiosked-adframe='outer']");
        findElement("#root_template_div");
        findElement("#singlebotad");
        findElement("#adunit");
        findElement("#sidebar");

        System.out.println("Number of ads: " + counter);
    }

    private void findElement(String selector) {
        try {
            driver.findElement(By.cssSelector(selector));
            counter++;
        } catch (Exception e) {
            //
        }
    }
}
