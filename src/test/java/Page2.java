import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Page2 extends Testing {

    Page2() {
        PageFactory.initElements(driver, this);
    }

    void numOfAds() {
        driver.get("http://moneyversed.com/valuable-items-in-abandoned-places/?as=799");
        List<WebElement> numberOfAds = driver.findElements(By.cssSelector("[id*='div-gpt-ad-']"));


        System.out.println("Number of ads: " + numberOfAds.size());
    }
}
